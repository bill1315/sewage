package com.idcloud.project.system.domain;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * 水厂表
 */
@Data
@TableName("water_sys_works")
public class SysWorks {

    /**
     * 水厂ID
     */
    @TableId
    private String worksUid;

    /**
     * 水厂编号
     */
    private String worksCode;

    /**
     * 水厂名称
     */
    private String worksName;

    /**
     * 水厂地址
     */
    private String worksAddress;

    /**
     * 经度
     */
    private String worksLng;

    /**
     * 纬度
     */
    private String worksLat;

    /**
     * 描述
     */
    private String worksDesc;

    /**
     * 厂长ID
     */
    private String userUid;

    /**
     * 水质标准ID
     */
    private String stdUid;
    /**
     * 排序
     * */
    private  int sort;
   /** 设备主管*/
    private  String equUser;
    /**工艺主管*/
    private String  craftUser;
    /**企业id*/
    private  String enterpriseUid;
}