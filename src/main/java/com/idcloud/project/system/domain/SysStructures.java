package com.idcloud.project.system.domain;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * 构筑物表
 */
@Data
@TableName("water_sys_structure")
public class SysStructures {


    /**
     * 构筑物ID
     */
    @TableId
    private String structUid;

    /**
     * 构筑物编码
     */
    private String structCode;

    /**
     * 构筑物名称
     */
    private String name;

    /**
     * 水厂id
     */


    
}