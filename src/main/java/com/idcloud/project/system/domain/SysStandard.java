package com.idcloud.project.system.domain;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * 水质标准表
 */
@Data
@TableName("water_sys_standard")
public class SysStandard {

    /**
     * 水质标准ID
     */
    @TableId
    private String stdUid;

    /**
     * 标准名称
     */
    private String stdName;

    /**
     * ph上限值
     */
    private Float phUpper;

    /**
     * ph下限值
     */
    private Float phLower;

    /**
     * cod上限值
     */
    private Float codUpper;

    /**
     * cod下限值
     */
    private Float codLower;

    /**
     * 氨氮上限值
     */
    private Float nh3Upper;

    /**
     * 氨氮下限值
     */
    private Float nh3Lower;

    /**
     * tn上限值
     */
    private Float tnUpper;

    /**
     * tn下限值
     */
    private Float tnLower;

    /**
     * tp上限值
     */
    private Float tpUpper;

    /**
     * tp下限值
     */
    private Float tpLower;
}