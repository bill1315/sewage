package com.idcloud.project.system.domain.dto;


import com.idcloud.project.system.domain.WorksUser;
import lombok.Data;

@Data
public class WorksUserDto extends WorksUser {
    String userName;
}
