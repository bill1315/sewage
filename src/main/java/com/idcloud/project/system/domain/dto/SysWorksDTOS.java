package com.idcloud.project.system.domain.dto;


import com.idcloud.project.system.domain.SysWorks;
import lombok.Data;

import java.util.List;

@Data
public class SysWorksDTOS extends SysWorks {
    String uid;
    String name;
    List<SysStructureDTOS> children;
    String desc;
    public List<SysStructureDTOS> getChildren() {
        return children;
    }

    public void setChildren(List<SysStructureDTOS> children) {
        this.children = children;
    }
}
