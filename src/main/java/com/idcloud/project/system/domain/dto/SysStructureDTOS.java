package com.idcloud.project.system.domain.dto;


import com.idcloud.project.system.domain.SysStructures;
import lombok.Data;

@Data
public class SysStructureDTOS extends SysStructures {

    String uid;
    String name;
   // List<EquInfoDTO>  children;
    String desc;
//    public List<EquInfoDTO> getChildren() {
//        return children;
//    }
//
//    public void setChildren(List<EquInfoDTO> children) {
//        this.children = children;
//    }
}
