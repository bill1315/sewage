package com.idcloud.project.system.domain;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

@Data
@TableName("water_works_user")
public class WorksUser {
    @TableId
    private  String wuid;
    private  String worksUid;
    private  String userUid;
}
