package com.idcloud.project.system.domain.dto;


import com.idcloud.project.system.domain.SysWorks;
import lombok.Data;

@Data
public class SysWorksDTO  extends SysWorks {
    String userName;
    String stdName;
    String equUserName;

    String craftUserName;

}
