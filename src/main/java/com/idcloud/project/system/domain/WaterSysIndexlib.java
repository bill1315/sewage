package com.idcloud.project.system.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.idcloud.framework.aspectj.lang.annotation.Excel;
import com.idcloud.framework.web.domain.BaseEntity;

/**
 * 指标对象 water_sys_indexlib
 * 
 * @author ruoyi
 * @date 2020-02-18
 */
public class WaterSysIndexlib extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 指标ID */
    private Long indexUid;

    /** 指标编码 */
    @Excel(name = "指标编码")
    private String indexCode;

    /** 指标名称 */
    @Excel(name = "指标名称")
    private String indexName;

    /** 指标单位 */
    @Excel(name = "指标单位")
    private String indexUnit;

    /** 指标类型 1为化验 2为仪表 */
    @Excel(name = "指标类型 1为化验 2为仪表")
    private Integer indexType;

    public void setIndexUid(Long indexUid) 
    {
        this.indexUid = indexUid;
    }

    public Long getIndexUid() 
    {
        return indexUid;
    }
    public void setIndexCode(String indexCode) 
    {
        this.indexCode = indexCode;
    }

    public String getIndexCode() 
    {
        return indexCode;
    }
    public void setIndexName(String indexName) 
    {
        this.indexName = indexName;
    }

    public String getIndexName() 
    {
        return indexName;
    }
    public void setIndexUnit(String indexUnit) 
    {
        this.indexUnit = indexUnit;
    }

    public String getIndexUnit() 
    {
        return indexUnit;
    }
    public void setIndexType(Integer indexType) 
    {
        this.indexType = indexType;
    }

    public Integer getIndexType() 
    {
        return indexType;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("indexUid", getIndexUid())
            .append("indexCode", getIndexCode())
            .append("indexName", getIndexName())
            .append("indexUnit", getIndexUnit())
            .append("indexType", getIndexType())
            .toString();
    }
}
