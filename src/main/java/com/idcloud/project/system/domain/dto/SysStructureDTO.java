package com.idcloud.project.system.domain.dto;


import com.idcloud.project.system.domain.SysStructure;
import lombok.Data;

@Data
public class SysStructureDTO extends SysStructure {

    String worksName;

//    List<EquInfo>  equinfo;


    public String getWorksName() {
        return worksName;
    }

    public void setWorksName(String worksName) {
        this.worksName = worksName;
    }
}
