package com.idcloud.project.system.controller;


import com.idcloud.common.utils.R;
import com.idcloud.project.system.domain.WorksUser;
import com.idcloud.project.system.service.WorksUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/user")
public class WorksUserController {
    @Autowired
    private WorksUserService service;

    @GetMapping("/findWorksUserDto")
    public R findWorksUserDto(@RequestParam String uid){
        return new R(service.findWorksUserDto(uid));
    }
    @PostMapping("/saveWorksUser")
    public R saveWorksUser(@RequestBody WorksUser worksUser){
        R r=new R();
        if (service.saveWorksUser(worksUser)){
                r.setCode(200);
        }else {
        r.setCode(500);
        }
        return  r;
    }

    @GetMapping("/delWorksUser")
    public R delWorksUser(@RequestParam String worksUser){
        R r=new R();
        if (service.delWorksUser(worksUser)){
            r.setCode(200);
        }else {
            r.setCode(500);
        }
        return  r;
    }
}
