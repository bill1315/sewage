package com.idcloud.project.system.controller;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.idcloud.framework.aspectj.lang.annotation.Log;
import com.idcloud.framework.aspectj.lang.enums.BusinessType;
import com.idcloud.project.system.domain.WaterSysIndexlib;
import com.idcloud.project.system.service.IWaterSysIndexlibService;
import com.idcloud.framework.web.controller.BaseController;
import com.idcloud.framework.web.domain.AjaxResult;
import com.idcloud.common.utils.poi.ExcelUtil;
import com.idcloud.framework.web.page.TableDataInfo;

/**
 * 指标Controller
 * 
 * @author ruoyi
 * @date 2020-02-18
 */
@RestController
@RequestMapping("/system/indexlib")
public class WaterSysIndexlibController extends BaseController
{
    @Autowired
    private IWaterSysIndexlibService waterSysIndexlibService;

    /**
     * 查询指标列表
     */
    @PreAuthorize("@ss.hasPermi('system:indexlib:list')")
    @GetMapping("/list")
    public TableDataInfo list(WaterSysIndexlib waterSysIndexlib)
    {
        startPage();
        List<WaterSysIndexlib> list = waterSysIndexlibService.selectWaterSysIndexlibList(waterSysIndexlib);
        return getDataTable(list);
    }

    /**
     * 导出指标列表
     */
    @PreAuthorize("@ss.hasPermi('system:indexlib:export')")
    @Log(title = "指标", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(WaterSysIndexlib waterSysIndexlib)
    {
        List<WaterSysIndexlib> list = waterSysIndexlibService.selectWaterSysIndexlibList(waterSysIndexlib);
        ExcelUtil<WaterSysIndexlib> util = new ExcelUtil<WaterSysIndexlib>(WaterSysIndexlib.class);
        return util.exportExcel(list, "indexlib");
    }

    /**
     * 获取指标详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:indexlib:query')")
    @GetMapping(value = "/{indexUid}")
    public AjaxResult getInfo(@PathVariable("indexUid") Long indexUid)
    {
        return AjaxResult.success(waterSysIndexlibService.selectWaterSysIndexlibById(indexUid));
    }

    /**
     * 新增指标
     */
    @PreAuthorize("@ss.hasPermi('system:indexlib:add')")
    @Log(title = "指标", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody WaterSysIndexlib waterSysIndexlib)
    {
        return toAjax(waterSysIndexlibService.insertWaterSysIndexlib(waterSysIndexlib));
    }

    /**
     * 修改指标
     */
    @PreAuthorize("@ss.hasPermi('system:indexlib:edit')")
    @Log(title = "指标", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody WaterSysIndexlib waterSysIndexlib)
    {
        return toAjax(waterSysIndexlibService.updateWaterSysIndexlib(waterSysIndexlib));
    }

    /**
     * 删除指标
     */
    @PreAuthorize("@ss.hasPermi('system:indexlib:remove')")
    @Log(title = "指标", businessType = BusinessType.DELETE)
	  @DeleteMapping("/{indexUids}")
    public AjaxResult remove(@PathVariable Long[] indexUids)
    {
        return toAjax(waterSysIndexlibService.deleteWaterSysIndexlibByIds(indexUids));
    }
}
