package com.idcloud.project.system.controller;

import com.idcloud.common.utils.R;

import com.idcloud.project.system.domain.SysStandard;
import com.idcloud.project.system.service.ISysStandardService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@RequestMapping("/sys")
public class SysStandardController {
    @Autowired
    private ISysStandardService sysStandardService;
    @GetMapping("/getSysStandardList")
    public R getSysStandardList(@RequestParam String name){
        return  new R(sysStandardService.getSysStandardList(name));
    }

    @PostMapping("/addSysStandard")
    public R addSysStandard(@RequestBody SysStandard sysStandard){
        R r=new R();
        if (sysStandardService.addSysStandard(sysStandard)){
            r.setCode(200);
        }else {
            r.setCode(500);
            r.setMsg("请求失败");
        }
        return r;
    }

    @GetMapping("/getSysStandard")
    public R getSysStandard(@RequestParam Map<String ,Object> map){
        return new R(sysStandardService.getSysStandard(map));
    }

    @GetMapping("/getSysStandardById")
    public R getSysStandardById(@RequestParam String uid){
        return new R(sysStandardService.getSysStandardById(uid));
    }
    @GetMapping("/checkSysName")
    public R checkSysName(@RequestParam String stdName){
        return new R(sysStandardService.checkSysName(stdName));
    }
    @GetMapping("/delSysStandard")
    public R delSysStandard(@RequestParam String uid){
        R r=new R();
        if (sysStandardService.delSysStandard(uid)){
            r.setCode(200);
        }else {
            r.setCode(500);
            r.setMsg("请求失败");
        }
        return r;
    }


}
