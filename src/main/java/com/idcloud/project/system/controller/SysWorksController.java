package com.idcloud.project.system.controller;


import com.idcloud.common.utils.R;
import com.idcloud.project.system.domain.SysWorks;
import com.idcloud.project.system.service.SysWorksService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@RequestMapping("/sys")
public class SysWorksController {

    @Autowired
    private SysWorksService sysWorksService;
    @GetMapping("/getWorksList")
    public R getWorksList(){
        return  new R(sysWorksService.getWorksList());
    }
    @PostMapping("/addsysWorks")
    public R addsysWorks(@RequestBody SysWorks sysWorks){
        R r=new R();
        if (sysWorksService.addWorks(sysWorks)){
           r.setCode(200);
        }else {
            r.setCode(500);
            r.setMsg("请求失败");
        }
        return r;
    }
    @GetMapping("/checkSysWorks")
    public R checkSysWorks(@RequestParam String  name){
        return new R(sysWorksService.checkSysWorks(name));
    }
    @GetMapping("/getWorks")
    public R getWorks(@RequestParam Map<String,Object> map){
        return new R(sysWorksService.getWorks(map));
    }

    @GetMapping("/getWorksById")
    public R getWorksById(@RequestParam String name){
        return new R(sysWorksService.getWorksById(name));
    }

    @GetMapping("/delWorks")
    public R delWorks(@RequestParam String uid){

        R r=new R();
        if (sysWorksService.delWorks(uid)){
            r.setCode(200);
        }else {
            r.setCode(500);
            r.setMsg("请求失败");
        }
        return r;
    }

    @PostMapping("/upWorks")
    public R upWorks(@RequestBody SysWorks sysWorks){
        R r=new R();
        if (sysWorksService.upWorks(sysWorks)){
            r.setCode(200);
        }else {
            r.setCode(500);
            r.setMsg("请求失败");
        }
        return r;
    }
}
