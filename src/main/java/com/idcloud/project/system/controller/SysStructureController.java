package com.idcloud.project.system.controller;


import com.idcloud.common.utils.R;
import com.idcloud.project.system.domain.SysStructure;
import com.idcloud.project.system.service.SysStructureService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@RequestMapping("/sys")
public class SysStructureController {
    @Autowired
    private SysStructureService sysStructureService;
    @GetMapping("/checkSysStructure")
    public R checkSysStructure(@RequestParam String  name){
        return new R(sysStructureService.checkSysStructure(name));
    }
    @GetMapping("/SysStructureByName")
    public R SysStructureByName(@RequestParam String sysname){
        return  new R(sysStructureService.SysStructureByName(sysname));
    }
    @GetMapping("/SysStructureList")
    public  R SysStructureList(@RequestParam String uid){
        return  new R(sysStructureService.SysStructureList(uid));
    }
    @PostMapping("/addSysStructure")
    public R addSysStructure(@RequestBody SysStructure sysStructure){
        R r=new R();
        if (sysStructureService.addSysStructure(sysStructure)){
            r.setCode(200);
        }else {
            r.setCode(500);
            r.setMsg("请求失败");
        }
        return r;
    }

    @GetMapping("/getSysStructure")
    public R getSysStructure(@RequestParam Map<String,Object> map){
        return new R(sysStructureService.getSysStructure(map));
    }

    @GetMapping("/getSysStructureById")
    public R getSysStructureById(@RequestParam String uid){
        return new R(sysStructureService.getSysStructureById(uid));
    }

    @GetMapping("/delSysStructureById")
    public R delSysStructureById(@RequestParam String uid){
        R r=new R();
        if (sysStructureService.delSysStructureById(uid)){
            r.setCode(200);
        }else {
            r.setCode(500);
            r.setMsg("请求失败");
        }
        return r;
    }

    @PostMapping("/upSysStructureById")
    public R upSysStructureById(@RequestBody SysStructure sysStructure){
        R r=new R();
        if (sysStructureService.upSysStructureById(sysStructure)){
            r.setCode(200);
        }else {
            r.setCode(500);
            r.setMsg("请求失败");
        }
        return r;
    }
}
