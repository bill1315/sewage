package com.idcloud.project.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import com.idcloud.project.system.domain.WorksUser;
import com.idcloud.project.system.domain.dto.WorksUserDto;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface WorksUserMapper extends BaseMapper<WorksUser> {

        List<WorksUserDto> findWorksUserDto(@Param("uid") String uid);
}
