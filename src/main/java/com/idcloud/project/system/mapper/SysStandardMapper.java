package com.idcloud.project.system.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

import com.idcloud.project.system.domain.SysStandard;
import org.apache.ibatis.annotations.Param;

import java.util.Map;

/**
 * 水质标准表 Mapper 接口
 */
public interface SysStandardMapper extends BaseMapper<SysStandard> {
    IPage getSysStandard(Page page, @Param("ew") Map<String, Object> map);
}