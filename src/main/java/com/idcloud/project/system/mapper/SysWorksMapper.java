package com.idcloud.project.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

import com.idcloud.project.system.domain.dto.SysWorksDTOS;
import com.idcloud.project.system.domain.SysWorks;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * 水厂表 Mapper 接口
 */
public interface SysWorksMapper extends BaseMapper<SysWorks> {
    IPage getWorks(Page page, @Param("ew") Map<String, Object> params);
    List<SysWorksDTOS> getWorksList();
}