package com.idcloud.project.system.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

import com.idcloud.common.utils.CommonUtil;
import com.idcloud.common.utils.Query;
import com.idcloud.project.system.domain.SysStructure;
import com.idcloud.project.system.mapper.SysStructureMapper;
import com.idcloud.project.system.service.SysStructureService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

@Service
@Transactional
public class SysStructureServiceImpl implements SysStructureService {
    @Autowired
    private SysStructureMapper sysStructureMapper;

    @Override
    public List<SysStructure> SysStructureList(String worksuid) {
        QueryWrapper<SysStructure> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("works_uid", worksuid);

        return sysStructureMapper.selectList(queryWrapper);
    }

    @Override
    public SysStructure getSysStructureById(String uid) {
        return sysStructureMapper.selectById(uid);
    }

    @Override
    public boolean addSysStructure(SysStructure sysStructure) {
        int i=0;
        if(CommonUtil.checkNull(sysStructure.getStructUid())){
                sysStructure.setStructUid(CommonUtil.getUID());
                i=sysStructureMapper.insert(sysStructure);
        }else {
            i=sysStructureMapper.updateById(sysStructure);
        }
        return i==0? false:true;
    }

    @Override
    public int checkSysStructure(String name) {
        return sysStructureMapper.selectCount(new QueryWrapper<SysStructure>().eq("struct_code",name));
    }

    @Override
    public IPage getSysStructure(Map<String, Object> map) {
        Page page=new Query(map);
        return sysStructureMapper.getSysStructure(page,map);
    }

    @Override
    public boolean delSysStructureById(String uid) {
        return sysStructureMapper.deleteById(uid)==0? false:true;
    }

    @Override
    public boolean upSysStructureById(SysStructure sysStructure) {
        return sysStructureMapper.updateById(sysStructure)==0? false:true;
    }

    @Override
    public List<SysStructure> SysStructureByName(String sysname) {
        QueryWrapper<SysStructure> queryWrapper;
        if (!sysname.equals("null") && !sysname.equals("")){
            queryWrapper=new QueryWrapper<>();
            queryWrapper.like("struct_name",sysname);
        }else {
            queryWrapper=new QueryWrapper<>(null);
        }
        return sysStructureMapper.selectList(queryWrapper);
    }
}
