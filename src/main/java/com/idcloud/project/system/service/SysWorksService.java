package com.idcloud.project.system.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.idcloud.project.system.domain.dto.SysWorksDTOS;
import com.idcloud.project.system.domain.SysWorks;


import java.util.List;
import java.util.Map;

public interface SysWorksService {
    IPage getWorks(Map<String, Object> params);
    SysWorks getWorksById(String wid);
    boolean addWorks(SysWorks works);
    List<SysWorksDTOS> getWorksList();
    boolean delWorks(String wid);
    int checkSysWorks(String name);
    boolean upWorks(SysWorks works);
}
