package com.idcloud.project.system.service.impl;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.idcloud.common.utils.CommonUtil;
import com.idcloud.common.utils.Query;

import com.idcloud.framework.web.domain.server.Sys;
import com.idcloud.project.system.domain.SysStandard;
import com.idcloud.project.system.mapper.SysStandardMapper;
import com.idcloud.project.system.service.ISysStandardService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

@Service
@Transactional
public class SysStandardServiceImpl implements ISysStandardService {

    @Autowired
    private SysStandardMapper sysStandardMapper;

    @Override
    public List<SysStandard> getSysStandardList(String stdName) {
        List<SysStandard> list = stdName.equals("null") ? sysStandardMapper.selectList(new QueryWrapper<SysStandard>(null)) : sysStandardMapper.selectList(new QueryWrapper<SysStandard>().like("std_name", stdName));
        return list;
    }

    @Override
    public IPage getSysStandard(Map<String, Object> map) {
        Page page = new Query(map);
        return sysStandardMapper.getSysStandard(page, map);
    }

    @Override
    public int checkSysName(String stdName) {
        return sysStandardMapper.selectCount(new QueryWrapper<SysStandard>().eq("std_name",stdName));
    }

    @Override
    public SysStandard getSysStandardById(String uid) {
        return sysStandardMapper.selectById(uid);
    }

    @Override
    public boolean addSysStandard(SysStandard sysStandard) {
        int i = 0;
        if (CommonUtil.checkNull(sysStandard.getStdUid())) {
            sysStandard.setStdUid(CommonUtil.getUID());
            System.out.println(sysStandard);
            i = sysStandardMapper.insert(sysStandard);
        } else {
            i = sysStandardMapper.updateById(sysStandard);
        }
        return i == 0 ? false : true;
    }

    @Override
    public boolean delSysStandard(String uid) {
        return sysStandardMapper.deleteById(uid) == 0 ? false : true;
    }


}
