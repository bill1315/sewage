package com.idcloud.project.system.service;



import com.idcloud.project.system.domain.WorksUser;
import com.idcloud.project.system.domain.dto.WorksUserDto;

import java.util.List;

public interface WorksUserService {
    boolean saveWorksUser(WorksUser worksUser);
    List<WorksUserDto> findWorksUserDto(String uid);
    boolean delWorksUser(String uid);
}
