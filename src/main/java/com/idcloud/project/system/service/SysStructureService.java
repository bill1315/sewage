package com.idcloud.project.system.service;



import com.baomidou.mybatisplus.core.metadata.IPage;
import com.idcloud.project.system.domain.SysStructure;


import java.util.List;
import java.util.Map;

public interface SysStructureService {

    IPage getSysStructure(Map<String, Object> map);

    SysStructure getSysStructureById(String uid);

    boolean addSysStructure(SysStructure sysStructure);

    boolean delSysStructureById(String uid);

    boolean upSysStructureById(SysStructure sysStructure);
    int checkSysStructure(String name);
    List<SysStructure> SysStructureList(String worksuid);

    List<SysStructure> SysStructureByName(String sysname);

}
