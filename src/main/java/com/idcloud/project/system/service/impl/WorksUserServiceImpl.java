package com.idcloud.project.system.service.impl;

import com.idcloud.common.utils.CommonUtil;
import com.idcloud.project.system.domain.WorksUser;
import com.idcloud.project.system.domain.dto.WorksUserDto;
import com.idcloud.project.system.mapper.WorksUserMapper;
import com.idcloud.project.system.service.WorksUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional
@Service
public class WorksUserServiceImpl implements WorksUserService {
    @Autowired
    private WorksUserMapper mapper;
    @Override
    public boolean saveWorksUser(WorksUser worksUser) {
        int i=0;
        if (CommonUtil.checkNull(worksUser.getWuid())){
            worksUser.setWuid(CommonUtil.getUID());
            i=mapper.insert(worksUser);
        }else {
            i=mapper.updateById(worksUser);
        }
        return i==0?false:true;
    }

    @Override
    public List<WorksUserDto> findWorksUserDto(String uid) {
        return  mapper.findWorksUserDto(uid);
    }



    @Override
    public boolean delWorksUser(String uid) {
        return mapper.deleteById(uid)==0?false:true;
    }
}
