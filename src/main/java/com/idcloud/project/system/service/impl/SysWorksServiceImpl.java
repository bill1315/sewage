package com.idcloud.project.system.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

import com.idcloud.common.utils.CommonUtil;
import com.idcloud.common.utils.Query;
import com.idcloud.common.utils.SecurityUtils;
import com.idcloud.project.system.domain.dto.SysWorksDTO;
import com.idcloud.project.system.domain.dto.SysWorksDTOS;
import com.idcloud.project.system.domain.SysWorks;
import com.idcloud.project.system.mapper.SysWorksMapper;
import com.idcloud.project.system.service.SysWorksService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

@Service
@Transactional
public class SysWorksServiceImpl implements SysWorksService {
    @Autowired
    private SysWorksMapper sysWorksMapper;


    //获取水厂数据
    @Override
    public IPage getWorks(Map<String,Object> params ) {
        Page<SysWorksDTO> page=new Query<>(params);
        IPage<SysWorksDTO> iPage=sysWorksMapper.getWorks(page,params);
        return iPage;
    }
    //获取全部数据
    @Override
    public List<SysWorksDTOS> getWorksList() {
        return sysWorksMapper.getWorksList();
    }

    @Override
    public int checkSysWorks(String name) {
        return sysWorksMapper.selectCount(new QueryWrapper<SysWorks>().eq("works_code",name));
    }

    @Override
    public SysWorks getWorksById(String wid) {
        return sysWorksMapper.selectById(wid);
    }

    @Override
    public boolean addWorks(SysWorks works) {
        int i=0;
        if(CommonUtil.checkNull(works.getWorksUid())){
            works.setWorksUid(CommonUtil.getUID());
            i=sysWorksMapper.insert(works);
        }else {
            i=sysWorksMapper.updateById(works);
        }
        return i==0?false:true;
    }

    @Override
    public boolean delWorks(String wid) {
        return sysWorksMapper.deleteById(wid)==0?false:true;
    }

    @Override
    public boolean upWorks(SysWorks works) {
        return sysWorksMapper.updateById(works)==0?false:true;
    }
}
