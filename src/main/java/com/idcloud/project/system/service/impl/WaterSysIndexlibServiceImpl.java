package com.idcloud.project.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.idcloud.project.system.mapper.WaterSysIndexlibMapper;
import com.idcloud.project.system.domain.WaterSysIndexlib;
import com.idcloud.project.system.service.IWaterSysIndexlibService;

/**
 * 指标Service业务层处理
 * 
 * @author ruoyi
 * @date 2020-02-18
 */
@Service
public class WaterSysIndexlibServiceImpl implements IWaterSysIndexlibService 
{
    @Autowired
    private WaterSysIndexlibMapper waterSysIndexlibMapper;

    /**
     * 查询指标
     * 
     * @param indexUid 指标ID
     * @return 指标
     */
    @Override
    public WaterSysIndexlib selectWaterSysIndexlibById(Long indexUid)
    {
        return waterSysIndexlibMapper.selectWaterSysIndexlibById(indexUid);
    }

    /**
     * 查询指标列表
     * 
     * @param waterSysIndexlib 指标
     * @return 指标
     */
    @Override
    public List<WaterSysIndexlib> selectWaterSysIndexlibList(WaterSysIndexlib waterSysIndexlib)
    {
        return waterSysIndexlibMapper.selectWaterSysIndexlibList(waterSysIndexlib);
    }

    /**
     * 新增指标
     * 
     * @param waterSysIndexlib 指标
     * @return 结果
     */
    @Override
    public int insertWaterSysIndexlib(WaterSysIndexlib waterSysIndexlib)
    {
        return waterSysIndexlibMapper.insertWaterSysIndexlib(waterSysIndexlib);
    }

    /**
     * 修改指标
     * 
     * @param waterSysIndexlib 指标
     * @return 结果
     */
    @Override
    public int updateWaterSysIndexlib(WaterSysIndexlib waterSysIndexlib)
    {
        return waterSysIndexlibMapper.updateWaterSysIndexlib(waterSysIndexlib);
    }

    /**
     * 批量删除指标
     * 
     * @param indexUids 需要删除的指标ID
     * @return 结果
     */
    @Override
    public int deleteWaterSysIndexlibByIds(Long[] indexUids)
    {
        return waterSysIndexlibMapper.deleteWaterSysIndexlibByIds(indexUids);
    }

    /**
     * 删除指标信息
     * 
     * @param indexUid 指标ID
     * @return 结果
     */
    @Override
    public int deleteWaterSysIndexlibById(Long indexUid)
    {
        return waterSysIndexlibMapper.deleteWaterSysIndexlibById(indexUid);
    }
}
