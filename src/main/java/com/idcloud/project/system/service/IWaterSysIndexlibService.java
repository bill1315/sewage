package com.idcloud.project.system.service;

import com.idcloud.project.system.domain.WaterSysIndexlib;
import java.util.List;

/**
 * 指标Service接口
 * 
 * @author ruoyi
 * @date 2020-02-18
 */
public interface IWaterSysIndexlibService 
{
    /**
     * 查询指标
     * 
     * @param indexUid 指标ID
     * @return 指标
     */
    public WaterSysIndexlib selectWaterSysIndexlibById(Long indexUid);

    /**
     * 查询指标列表
     * 
     * @param waterSysIndexlib 指标
     * @return 指标集合
     */
    public List<WaterSysIndexlib> selectWaterSysIndexlibList(WaterSysIndexlib waterSysIndexlib);

    /**
     * 新增指标
     * 
     * @param waterSysIndexlib 指标
     * @return 结果
     */
    public int insertWaterSysIndexlib(WaterSysIndexlib waterSysIndexlib);

    /**
     * 修改指标
     * 
     * @param waterSysIndexlib 指标
     * @return 结果
     */
    public int updateWaterSysIndexlib(WaterSysIndexlib waterSysIndexlib);

    /**
     * 批量删除指标
     * 
     * @param indexUids 需要删除的指标ID
     * @return 结果
     */
    public int deleteWaterSysIndexlibByIds(Long[] indexUids);

    /**
     * 删除指标信息
     * 
     * @param indexUid 指标ID
     * @return 结果
     */
    public int deleteWaterSysIndexlibById(Long indexUid);
}
