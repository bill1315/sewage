package com.idcloud.project.system.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.idcloud.project.system.domain.SysStandard;


import java.util.List;
import java.util.Map;

public interface ISysStandardService {
    IPage getSysStandard(Map<String, Object> map);
    List<SysStandard> getSysStandardList(String stdName);
    SysStandard getSysStandardById(String uid);

    boolean addSysStandard(SysStandard sysStandard);

    boolean delSysStandard(String uid);

    int checkSysName(String stdName);
}