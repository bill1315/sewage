package com.idcloud.project.process.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.idcloud.project.process.mapper.WaterPatrolTaskMapper;
import com.idcloud.project.process.domain.WaterPatrolTask;
import com.idcloud.project.process.service.IWaterPatrolTaskService;

/**
 * 巡视任务Service业务层处理
 * 
 * @author ruoyi
 * @date 2020-02-21
 */
@Service
public class WaterPatrolTaskServiceImpl implements IWaterPatrolTaskService 
{
    @Autowired
    private WaterPatrolTaskMapper waterPatrolTaskMapper;

    /**
     * 查询巡视任务
     * 
     * @param taskUid 巡视任务ID
     * @return 巡视任务
     */
    @Override
    public WaterPatrolTask selectWaterPatrolTaskById(String taskUid)
    {
        return waterPatrolTaskMapper.selectWaterPatrolTaskById(taskUid);
    }

    /**
     * 查询巡视任务列表
     * 
     * @param waterPatrolTask 巡视任务
     * @return 巡视任务
     */
    @Override
    public List<WaterPatrolTask> selectWaterPatrolTaskList(WaterPatrolTask waterPatrolTask)
    {
        return waterPatrolTaskMapper.selectWaterPatrolTaskList(waterPatrolTask);
    }

    /**
     * 新增巡视任务
     * 
     * @param waterPatrolTask 巡视任务
     * @return 结果
     */
    @Override
    public int insertWaterPatrolTask(WaterPatrolTask waterPatrolTask)
    {
        return waterPatrolTaskMapper.insertWaterPatrolTask(waterPatrolTask);
    }

    /**
     * 修改巡视任务
     * 
     * @param waterPatrolTask 巡视任务
     * @return 结果
     */
    @Override
    public int updateWaterPatrolTask(WaterPatrolTask waterPatrolTask)
    {
        return waterPatrolTaskMapper.updateWaterPatrolTask(waterPatrolTask);
    }

    /**
     * 批量删除巡视任务
     * 
     * @param taskUids 需要删除的巡视任务ID
     * @return 结果
     */
    @Override
    public int deleteWaterPatrolTaskByIds(String[] taskUids)
    {
        return waterPatrolTaskMapper.deleteWaterPatrolTaskByIds(taskUids);
    }

    /**
     * 删除巡视任务信息
     * 
     * @param taskUid 巡视任务ID
     * @return 结果
     */
    @Override
    public int deleteWaterPatrolTaskById(String taskUid)
    {
        return waterPatrolTaskMapper.deleteWaterPatrolTaskById(taskUid);
    }
}
