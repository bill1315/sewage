package com.idcloud.project.process.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.idcloud.project.process.mapper.WaterPatrolPlanMapper;
import com.idcloud.project.process.domain.WaterPatrolPlan;
import com.idcloud.project.process.service.IWaterPatrolPlanService;

/**
 * 巡视计划Service业务层处理
 * 
 * @author ruoyi
 * @date 2020-02-21
 */
@Service
public class WaterPatrolPlanServiceImpl implements IWaterPatrolPlanService 
{
    @Autowired
    private WaterPatrolPlanMapper waterPatrolPlanMapper;

    /**
     * 查询巡视计划
     * 
     * @param planUid 巡视计划ID
     * @return 巡视计划
     */
    @Override
    public WaterPatrolPlan selectWaterPatrolPlanById(String planUid)
    {
        return waterPatrolPlanMapper.selectWaterPatrolPlanById(planUid);
    }

    /**
     * 查询巡视计划列表
     * 
     * @param waterPatrolPlan 巡视计划
     * @return 巡视计划
     */
    @Override
    public List<WaterPatrolPlan> selectWaterPatrolPlanList(WaterPatrolPlan waterPatrolPlan)
    {
        return waterPatrolPlanMapper.selectWaterPatrolPlanList(waterPatrolPlan);
    }

    /**
     * 新增巡视计划
     * 
     * @param waterPatrolPlan 巡视计划
     * @return 结果
     */
    @Override
    public int insertWaterPatrolPlan(WaterPatrolPlan waterPatrolPlan)
    {
        return waterPatrolPlanMapper.insertWaterPatrolPlan(waterPatrolPlan);
    }

    /**
     * 修改巡视计划
     * 
     * @param waterPatrolPlan 巡视计划
     * @return 结果
     */
    @Override
    public int updateWaterPatrolPlan(WaterPatrolPlan waterPatrolPlan)
    {
        return waterPatrolPlanMapper.updateWaterPatrolPlan(waterPatrolPlan);
    }

    /**
     * 批量删除巡视计划
     * 
     * @param planUids 需要删除的巡视计划ID
     * @return 结果
     */
    @Override
    public int deleteWaterPatrolPlanByIds(String[] planUids)
    {
        return waterPatrolPlanMapper.deleteWaterPatrolPlanByIds(planUids);
    }

    /**
     * 删除巡视计划信息
     * 
     * @param planUid 巡视计划ID
     * @return 结果
     */
    @Override
    public int deleteWaterPatrolPlanById(String planUid)
    {
        return waterPatrolPlanMapper.deleteWaterPatrolPlanById(planUid);
    }
}
