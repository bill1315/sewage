package com.idcloud.project.process.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.idcloud.framework.aspectj.lang.annotation.Excel;
import com.idcloud.framework.web.domain.BaseEntity;
import java.util.Date;

/**
 * 巡视任务对象 water_patrol_task
 * 
 * @author ruoyi
 * @date 2020-02-21
 */
public class WaterPatrolTask extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 巡检任务ID */
    private String taskUid;

    /** 巡检计划ID */
    @Excel(name = "巡检计划ID")
    private String planUid;

    private WaterPatrolPlan plan;

    /** 巡检时间 */
    @Excel(name = "巡检时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date taskTime;

    public void setTaskUid(String taskUid) 
    {
        this.taskUid = taskUid;
    }

    public String getTaskUid() 
    {
        return taskUid;
    }
    public void setPlanUid(String planUid) 
    {
        this.planUid = planUid;
    }

    public String getPlanUid() 
    {
        return planUid;
    }
    public void setTaskTime(Date taskTime) 
    {
        this.taskTime = taskTime;
    }

    public Date getTaskTime() 
    {
        return taskTime;
    }

    public WaterPatrolPlan getPlan() {
        return plan;
    }

    public void setPlan(WaterPatrolPlan plan) {
        this.plan = plan;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("taskUid", getTaskUid())
            .append("planUid", getPlanUid())
            .append("taskTime", getTaskTime())
            .toString();
    }
}
