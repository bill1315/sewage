package com.idcloud.project.process.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.idcloud.framework.aspectj.lang.annotation.Excel;
import com.idcloud.framework.web.domain.BaseEntity;

/**
 * 巡视计划对象 water_patrol_plan
 * 
 * @author ruoyi
 * @date 2020-02-21
 */
public class WaterPatrolPlan extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 巡视ID */
    private String planUid;

    /** 巡视名称 */
    @Excel(name = "巡视名称")
    private String planName;

    /** 水厂ID */
    @Excel(name = "水厂ID")
    private String worksUid;

    /** 巡视类型 */
    @Excel(name = "巡视类型")
    private Long planType;

    /** 巡视频率 */
    @Excel(name = "巡视频率")
    private String planFreq;

    public void setPlanUid(String planUid) 
    {
        this.planUid = planUid;
    }

    public String getPlanUid() 
    {
        return planUid;
    }
    public void setPlanName(String planName) 
    {
        this.planName = planName;
    }

    public String getPlanName() 
    {
        return planName;
    }
    public void setWorksUid(String worksUid) 
    {
        this.worksUid = worksUid;
    }

    public String getWorksUid() 
    {
        return worksUid;
    }
    public void setPlanType(Long planType) 
    {
        this.planType = planType;
    }

    public Long getPlanType() 
    {
        return planType;
    }
    public void setPlanFreq(String planFreq) 
    {
        this.planFreq = planFreq;
    }

    public String getPlanFreq() 
    {
        return planFreq;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("planUid", getPlanUid())
            .append("planName", getPlanName())
            .append("worksUid", getWorksUid())
            .append("planType", getPlanType())
            .append("planFreq", getPlanFreq())
            .toString();
    }
}
