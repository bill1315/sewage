package com.idcloud.project.process.controller;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.idcloud.framework.aspectj.lang.annotation.Log;
import com.idcloud.framework.aspectj.lang.enums.BusinessType;
import com.idcloud.project.process.domain.WaterPatrolPlan;
import com.idcloud.project.process.service.IWaterPatrolPlanService;
import com.idcloud.framework.web.controller.BaseController;
import com.idcloud.framework.web.domain.AjaxResult;
import com.idcloud.common.utils.poi.ExcelUtil;
import com.idcloud.framework.web.page.TableDataInfo;

/**
 * 巡视计划Controller
 * 
 * @author ruoyi
 * @date 2020-02-21
 */
@RestController
@RequestMapping("/system/plan")
public class WaterPatrolPlanController extends BaseController
{
    @Autowired
    private IWaterPatrolPlanService waterPatrolPlanService;

    /**
     * 查询巡视计划列表
     */
    @PreAuthorize("@ss.hasPermi('system:plan:list')")
    @GetMapping("/list")
    public TableDataInfo list(WaterPatrolPlan waterPatrolPlan)
    {
        startPage();
        List<WaterPatrolPlan> list = waterPatrolPlanService.selectWaterPatrolPlanList(waterPatrolPlan);
        return getDataTable(list);
    }

    /**
     * 导出巡视计划列表
     */
    @PreAuthorize("@ss.hasPermi('system:plan:export')")
    @Log(title = "巡视计划", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(WaterPatrolPlan waterPatrolPlan)
    {
        List<WaterPatrolPlan> list = waterPatrolPlanService.selectWaterPatrolPlanList(waterPatrolPlan);
        ExcelUtil<WaterPatrolPlan> util = new ExcelUtil<WaterPatrolPlan>(WaterPatrolPlan.class);
        return util.exportExcel(list, "plan");
    }

    /**
     * 获取巡视计划详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:plan:query')")
    @GetMapping(value = "/{planUid}")
    public AjaxResult getInfo(@PathVariable("planUid") String planUid)
    {
        return AjaxResult.success(waterPatrolPlanService.selectWaterPatrolPlanById(planUid));
    }

    /**
     * 新增巡视计划
     */
    @PreAuthorize("@ss.hasPermi('system:plan:add')")
    @Log(title = "巡视计划", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody WaterPatrolPlan waterPatrolPlan)
    {
        return toAjax(waterPatrolPlanService.insertWaterPatrolPlan(waterPatrolPlan));
    }

    /**
     * 修改巡视计划
     */
    @PreAuthorize("@ss.hasPermi('system:plan:edit')")
    @Log(title = "巡视计划", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody WaterPatrolPlan waterPatrolPlan)
    {
        return toAjax(waterPatrolPlanService.updateWaterPatrolPlan(waterPatrolPlan));
    }

    /**
     * 删除巡视计划
     */
    @PreAuthorize("@ss.hasPermi('system:plan:remove')")
    @Log(title = "巡视计划", businessType = BusinessType.DELETE)
	@DeleteMapping("/{planUids}")
    public AjaxResult remove(@PathVariable String[] planUids)
    {
        return toAjax(waterPatrolPlanService.deleteWaterPatrolPlanByIds(planUids));
    }
}
