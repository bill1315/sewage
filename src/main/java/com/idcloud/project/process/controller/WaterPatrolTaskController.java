package com.idcloud.project.process.controller;

import com.idcloud.framework.web.controller.BaseController;
import com.idcloud.framework.web.page.TableDataInfo;
import com.idcloud.project.process.domain.WaterPatrolTask;
import com.idcloud.project.process.service.IWaterPatrolTaskService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 巡视任务Controller
 *
 * @author ruoyi
 * @date 2020-02-21
 */
@RestController
@RequestMapping("/system/task")
public class WaterPatrolTaskController extends BaseController {

  @Autowired
  private IWaterPatrolTaskService waterPatrolTaskService;

  /**
   * 查询巡视任务列表
   */
  @PreAuthorize("@ss.hasPermi('system:task:list')")
  @GetMapping("/list")
  public TableDataInfo list(WaterPatrolTask waterPatrolTask) {
    startPage();
    List<WaterPatrolTask> list = waterPatrolTaskService.selectWaterPatrolTaskList(waterPatrolTask);
    return getDataTable(list);
  }
}
