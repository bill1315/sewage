package com.idcloud.project.process.mapper;

import com.idcloud.project.process.domain.WaterPatrolTask;
import java.util.List;

/**
 * 巡视任务Mapper接口
 * 
 * @author ruoyi
 * @date 2020-02-21
 */
public interface WaterPatrolTaskMapper 
{
    /**
     * 查询巡视任务
     * 
     * @param taskUid 巡视任务ID
     * @return 巡视任务
     */
    public WaterPatrolTask selectWaterPatrolTaskById(String taskUid);

    /**
     * 查询巡视任务列表
     * 
     * @param waterPatrolTask 巡视任务
     * @return 巡视任务集合
     */
    public List<WaterPatrolTask> selectWaterPatrolTaskList(WaterPatrolTask waterPatrolTask);

    /**
     * 新增巡视任务
     * 
     * @param waterPatrolTask 巡视任务
     * @return 结果
     */
    public int insertWaterPatrolTask(WaterPatrolTask waterPatrolTask);

    /**
     * 修改巡视任务
     * 
     * @param waterPatrolTask 巡视任务
     * @return 结果
     */
    public int updateWaterPatrolTask(WaterPatrolTask waterPatrolTask);

    /**
     * 删除巡视任务
     * 
     * @param taskUid 巡视任务ID
     * @return 结果
     */
    public int deleteWaterPatrolTaskById(String taskUid);

    /**
     * 批量删除巡视任务
     * 
     * @param taskUids 需要删除的数据ID
     * @return 结果
     */
    public int deleteWaterPatrolTaskByIds(String[] taskUids);
}
