package com.idcloud.project.process.mapper;

import com.idcloud.project.process.domain.WaterPatrolPlan;
import java.util.List;

/**
 * 巡视计划Mapper接口
 * 
 * @author ruoyi
 * @date 2020-02-21
 */
public interface WaterPatrolPlanMapper 
{
    /**
     * 查询巡视计划
     * 
     * @param planUid 巡视计划ID
     * @return 巡视计划
     */
    public WaterPatrolPlan selectWaterPatrolPlanById(String planUid);

    /**
     * 查询巡视计划列表
     * 
     * @param waterPatrolPlan 巡视计划
     * @return 巡视计划集合
     */
    public List<WaterPatrolPlan> selectWaterPatrolPlanList(WaterPatrolPlan waterPatrolPlan);

    /**
     * 新增巡视计划
     * 
     * @param waterPatrolPlan 巡视计划
     * @return 结果
     */
    public int insertWaterPatrolPlan(WaterPatrolPlan waterPatrolPlan);

    /**
     * 修改巡视计划
     * 
     * @param waterPatrolPlan 巡视计划
     * @return 结果
     */
    public int updateWaterPatrolPlan(WaterPatrolPlan waterPatrolPlan);

    /**
     * 删除巡视计划
     * 
     * @param planUid 巡视计划ID
     * @return 结果
     */
    public int deleteWaterPatrolPlanById(String planUid);

    /**
     * 批量删除巡视计划
     * 
     * @param planUids 需要删除的数据ID
     * @return 结果
     */
    public int deleteWaterPatrolPlanByIds(String[] planUids);
}
